from argparse import ArgumentParser
import os
from datetime import datetime, timedelta
import pandas as pd
from string import ascii_letters
import unicodedata


def strip_accents(s):
    return "".join(
        c for c in unicodedata.normalize("NFD", s) if unicodedata.category(c) != "Mn"
    )


folder_name = os.path.join("content", "boeken")
AUDIOBOOK_MARKER = "(LB)"


def slugify(text):
    text = strip_accents(text)
    text = "".join([i if i in ascii_letters else "-" for i in text.strip()])
    while "--" in text:
        text = text.replace("--", "-")
    return text.lower()


def generate_page(year_i, year, title, author):
    is_audio = AUDIOBOOK_MARKER in author
    if is_audio:
        author = author.replace(AUDIOBOOK_MARKER, "")
    filename = slugify(f"{title}-{author}") + "-" + str(year) + ".md"
    fictional_date = datetime(year, 1, 1) + timedelta(days=year_i)
    image_filename = slugify(row_dict['title'] + "-" + row_dict['author']) + ".jpg"
    featured_image = ""
    if os.path.exists(f'content/boeken/{image_filename}'):
        featured_image = f'featured_image = "/boeken/{image_filename}"\n'
    md_content = (
        f"+++\n"
        f'title= "{title}"\n'
        f'author= "{author}"\n'
        f'date= {fictional_date.strftime("%Y-%m-%dT%H:%M:%S")}# fictional date\n'
        f'description= ""\n'
        f'{featured_image}'
        f"tags= []\n"
        f"audiobook= {str(is_audio).lower()}\n"
        f"draft= false\n"
        f"+++\n\n"
    )
    with open(os.path.join(folder_name, filename), "w") as file:
        file.write(md_content)


if __name__ == "__main__":
    parser = ArgumentParser(
        description="create pages in bulk",
    )
    parser.add_argument("csv_file")
    args = parser.parse_args()
    df = pd.read_csv(args.csv_file, quotechar='"', skipinitialspace=True)
    cur_year = None
    for row_dict in df.to_dict(orient="records"):
        if cur_year != row_dict["year"]:
            year_i = 0
            cur_year = row_dict["year"]
        else:
            year_i += 1
        row_dict["year_i"] = year_i
        generate_page(**row_dict)
