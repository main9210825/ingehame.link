import unicodedata
import re
import os
from datetime import datetime
from argparse import ArgumentParser

folder_name = os.path.join("content", "nachtwacht")
# Ensure the target folder exists
if not os.path.exists(folder_name):
    os.makedirs(folder_name)

def is_image_filename(filename):
    image_extensions = ['jpg', 'png', 'jpeg']
    _, extension = os.path.splitext(filename)
    extension = extension[1:].lower()
    return extension in image_extensions
    


def generate_page(image_filename):
    date_string = image_filename[4:12]
    date = datetime.strptime(date_string, "%Y%m%d").date()
    filename = "nachtwacht-" + date.strftime("%Y-%m-%d") + ".md"
    md_content = (
        f'+++\n'
        f'title= "nachtwacht update {date.strftime("%d-%m-%Y")}"\n'
        f'date= {date.strftime("%Y-%m-%dT%H:%M:%S")}\n'
        f'description= ""\n'
        f'featured_image = "/nachtwacht/{image_filename}"\n'
        f'tags= []\n'
        f'draft= false\n'
        f'+++\n\n'
        f'{{{{< figure src="/nachtwacht/{image_filename}" >}}}}'
    )
    with open(os.path.join(folder_name, filename), "w") as file:
        file.write(md_content)


if __name__ == "__main__":
    parser = ArgumentParser(
        description="create pages in bulk",
    )
    parser.add_argument("target")
    # parser.add_argument("--nachtwacht_photo", required=True)
    args = parser.parse_args()
    if os.path.isdir(args.target):
        image_files = [f for f in os.listdir(args.target) if is_image_filename(f)]
        for image_filename in image_files:
            generate_page(os.path.basename(image_filename))
    else:
        generate_page(os.path.basename(args.target))
