+++
title= "nachtwacht update 02-08-2024"
date= 2024-08-02T00:00:00
description= ""
featured_image = "/nachtwacht/IMG_20240802_003343138.jpg"
tags= []
draft= false
af=14
+++
Tijdens de vakantie in Hauzenberg heb ik het 14e vakje van het borduurwerk afgerond. De hellebaard is nu klaar en het begin van de vlag is gemaakt. Na een aantal vakjes met gezichten en andere details heb ik er nu wel weer zin in om wat achtergrond te borduren. Gelukkig koos ik er vanaf het begin voor om verticaal te gaan werken. Zo zit er veel variatie in het borduren.
Hier in Duitsland heb ik ook weer tijd om de biografie van Vasalis te lezen en wat is dat genieten. Twee jaar geleden kocht ik die bij de tweedehandsafdeling van Van der Velde, omdat Vasalis in havo 5 wordt behandeld. Haar gevoelsleven, haar gedachten en overpeinzingen zijn erg herkenbaar en het is genieten om die herkenning te vinden in een (non-fictie)boek. Intrigerend vind ik het dat zij zo weinig gepubliceerd heeft en daar haar hele leven mee geworsteld heeft. Haar belemmeringen zijn opgetekend in die biografie en die zijn erg voorstelbaar. Kortom: tijdens het borduren van dit gedeelte kon ik in de stilte van het huisje lang nadenken over de dag en de gedeeltes die ik heb gelezen. Ook over de kinderen en de vakantie op zichzelf kon ik nadenken: doen we leuke dingen? wat is het fijn om veel tijd met ze door te brengen, zie ik hun persoon genoeg? wat hebben ze leuke wijze uitspraken en wat is het fijn dat Krijn weinig woede heeft (maar hij is nog snel boos, maar ook erg lief voor Wiebe. Misschien heeft het te maken met vermoeidheid? Ik vind hem nog niet helemaal ontspannen. Wiebe daarentegen wel, heerlijk zonnekind.), moet ik meer contact leggen met de eigenaren van het vakantiehuisje?
Ondertussen staat mijn telefoon uit en ook dat vind ik erg fijn en rustig. En ik probeer niet de hele tijd toe te leven naar het einde, want vakantie is zo fijn. 

Vasalis had veel (literaire) contacten. Regelmatig kwam er iemand logeren. Ze had een innige band met hen. Doet me er wel aan denken hoe het mijn contacten en vriendschappen is. Zonderen we ons te veel af? Of komt dat wel wanneer de kinderen groot zijn? Het lijkt me heerlijk om avonden met mensen te praten over literatuur of andere hartszaken. Diepgaande gesprekken, mijmeringen.

{{< figure src="/nachtwacht/IMG_20240802_003343138.jpg" >}}