+++
title= "Nachtwacht update 09-06-2024"
date= 2024-06-09T00:00:00
description= ""
featured_image = "/nachtwacht/IMG_20240609_083630209.jpg"
tags= []
draft= false
af=12
+++
Sneller dan verwacht heb ik dit vakje afgerond, want er was eindelijk weer actie! De rode zijde van een persoon en aan het einde van het vakje, rechts bovenaan, een hand, in een [ponjet](https://static.kunstelo.nl/ckv2/cultuurwijs/cultuurwijs/www/nwc.rijksmuseumamsterdam/cultuurwijs.nl/i000710.html) (manchet). Er waren veel nieuwe kleuren nodig op deze pagina. Dat werkt stimulerend. Soms ook wel vertragend als je voor twee of drie vakjes een nieuw kleurtje nodig hebt. Tot nu toe klopt alles nog wat in het pakket zit. Bij mijn vorige [borduurwerk](https://www.rijksmuseum.nl/nl/collectie/SK-C-214) was er een kleur verkeerd gematcht met een symbool. Daar heb ik wel wat werk aan gehad om dat te herstellen. 

In het volgende vakje ga ik maar liefst drie gezichten tegenkomen. Dat zullen ook wel de laatste gezichten zijn die ik maak in dit borduurjaar. Zin in!
{{< figure src="/nachtwacht/IMG_20240609_083630209.jpg" >}}