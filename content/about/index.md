---
title: "Over mij"
description: "Ik kan me echt verliezen in het borduren van een kunstwerk."
# featured_image: '/images/Victor_Hugo-Hunchback.jpg'
menu:
  main:
    weight: 1
draft: true
---
Tellen, goed kijken, niet afgeleid raken. Sommige mensen denken dat borduren saai is, maar niets is minder waar. Eén verkeerde tel, en alles is in de war. Er staat veel op het spel.